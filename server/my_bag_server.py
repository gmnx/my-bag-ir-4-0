#!/usr/bin/env python
from flask import Flask, jsonify, abort, request, make_response, url_for, render_template, redirect
from gevent.pywsgi import WSGIServer
from datetime import datetime
import sys
import threading
import time
import atexit
import requests
import json

import sqlite3
conn = sqlite3.connect('mybag_db.sqlite', check_same_thread=False)
conn.row_factory = sqlite3.Row
cursor = conn.cursor()

POOL_TIME = 1 #Seconds
LOG_PATH = "/home/root/logs" #for testing using this path "/home/gmnx/emlid_server/logs"

# variables that are accessible from anywhere
api_auth_data = {}
user_auth_data = {}
device_auth_data={}
device_status_data={}
auth_success = False
start = 0
# lock to control access to variable
dataLock = threading.Lock()
# thread handler
xpandReaderThread = threading.Thread()

def create_app():
    app = Flask(__name__, static_url_path='/static')

    def interrupt():
        global xpandReaderThread
        xpandReaderThread.cancel()

    def api_auth():
        global auth_success
        url = "https://iot.xpand.asia/developer/api/applicationmgt/authenticate"

        payload = ""
        headers = {
            'X-Secret': "YjdFblc5QURQZjhOUE1VQ2JDNGZjUnBsdkJVYTptZTdrdl8yTmNoc3R6cG9TcTVsUVVlSWg4b29h",
            'cache-control': "no-cache",
            'Postman-Token': "f8645207-0dc9-424e-bf30-2bfd10dd85f5"
            }

        response = requests.request("GET", url, data=payload, headers=headers)
        if response.status_code == 200:
            auth_success = True
        else :
            auth_success = False
        print(response.text)
        return json.loads(response.text)

    def user_auth():
        url = "https://iot.xpand.asia/developer/api/usermgt/v1/authenticate"

        payload = "{\n  \"username\": \"UTMGroup11@noreply.com\",\n  \"password\": \"PecellelE\"\n}"
        headers = {
            'Authorization': "Bearer "+api_auth_data['access_token'],
            'Content-Type': "application/json",
            'cache-control': "no-cache",
            'Postman-Token': "5809db0e-2f1b-450d-b507-d0e979fa80c7"
            }

        response = requests.request("POST", url, data=payload, headers=headers)

        print(response.text)
        return json.loads(response.text)

    def device_auth():
        url = "https://iot.xpand.asia/developer/api/userdevicemgt/v1/devices"

        payload = ""
        headers = {
            'Authorization': "Bearer "+api_auth_data['access_token'],
            'X-IoT-JWT': user_auth_data['X-IoT-JWT'],
            'cache-control': "no-cache",
            'Postman-Token': "3d3749dc-74d8-4da4-9889-51f36b1afc36"
            }

        response = requests.request("GET", url, data=payload, headers=headers)

        print(response.text)
        return json.loads(response.text)

    def device_status():
        global auth_success
        url = "https://iot.xpand.asia/developer/api/userdevicemgt/v1/devices/15836/status"

        payload = ""
        headers = {
            'Authorization': "Bearer "+api_auth_data['access_token'],
            'X-IoT-JWT': user_auth_data['X-IoT-JWT'],
            'deviceId': str(device_auth_data[0]['id']),
            'cache-control': "no-cache",
            'Postman-Token': "c56815b9-8ddf-4e75-85a1-5da589d39503"
            }

        response = requests.request("GET", url, data=payload, headers=headers)
        if response.status_code == 200:
            auth_success = True
        else :
            auth_success = False
        #print(response.text)
        return json.loads(response.text)

    def insert_value(device):
        oxygen = 'null'
        ph = 'null'
        temp = 'null'
        date_taken = 'null'
        for sensor in device:
            if sensor['parameter']=='dissolvedOxygen':
                oxygen = sensor['value']
                date_taken = sensor['time']
            if sensor['parameter']=='ph':
                ph = sensor['value']
                date_taken = sensor['time']
            if sensor['parameter']=='temp':
                temp = sensor['value']
                date_taken = sensor['time']

        if 'null' in (oxygen, ph, temp, date_taken):
            return

        cursor.execute('SELECT * FROM data_table WHERE date_taken=?', (date_taken,))
        entry = cursor.fetchone()

        if entry is None:
            cursor.execute('INSERT INTO data_table (temperature,ph,oxygen,date_taken) VALUES (?,?,?,?)', (temp, ph, oxygen, date_taken))
            conn.commit()

    def doStuff():
        global api_auth_data
        global user_auth_data
        global device_auth_data
        global device_status_data
        global xpandReaderThread
        global start
        
        try :
            if not api_auth_data:
                api_auth_data = api_auth()
                api_auth_data['start'] = time.time()
            if not user_auth_data:
                user_auth_data =  user_auth()
            if not device_auth_data:
                device_auth_data =  device_auth()
            if not device_status_data:
                device_status_data = device_status()
            if 'start' in api_auth_data:
                elapsed = time.time() - api_auth_data['start']
                if (elapsed > api_auth_data['expires_in']) or (api_auth_data['expires_in']>3500) or not auth_success: #there is bug in xpand platform. the expires value will drop to 3299 after second request. 
                    api_auth_data = api_auth()
                    api_auth_data['start'] = time.time()
                    user_auth_data =  user_auth()
                    device_auth_data =  device_auth()
            if auth_success:
                device_status_data = device_status()
                if auth_success:
                    insert_value(device_status_data)
        except Exception as e:
            print e.__doc__
            print e.message

        # Set the next thread to happen
        xpandReaderThread = threading.Timer(POOL_TIME, doStuff, ())
        xpandReaderThread.start()

    def doStuffStart():
        # Do initialisation stuff here
        global xpandReaderThread
        # Create your thread
        xpandReaderThread = threading.Timer(POOL_TIME, doStuff, ())
        xpandReaderThread.start()

    # Initiate
    doStuffStart()
    # When you kill Flask (SIGTERM), clear the trigger for the next thread
    atexit.register(interrupt)
    return app

app = create_app()
app.secret_key = 'kentang'



def init_db():
    cursor.execute('''CREATE TABLE IF NOT EXISTS data_table
    (id INTEGER PRIMARY KEY,
    temperature     TEXT    NOT NULL,
    ph              TEXT    NOT NULL,
    oxygen          TEXT    NOT NULL,
    date_taken      TIMESTAMP    NOT NULL)''')
    conn.commit()


@app.route('/')
def index():
    cursor.execute('SELECT * FROM data_table ORDER BY date_taken DESC Limit 1')
    entry = cursor.fetchone()
    temp = entry['temperature']
    oxygen = entry['oxygen']
    ph = entry['ph']

    #The lower and upper lethal temperatures for Nile tilapia are 11-12 °C and 42 °C, respectively, while the preferred temperature ranges from 31 to 36 °C.
    temp_info="Good"
    temp_icon="check_box"
    if (float(temp)<12) or (float(temp)>42):
        temp_info = "Dangerous"
        temp_icon="warning"
    elif (float(temp)>24) and (float(temp)<36):
        temp_info = "Best"
        temp_icon="thumb_up_alt"

    #Tilapia are able to tolerate dissolved oxygen levels less than 0.3 mg/L, maintaining oxygen levels above 4 mg/L is preferable.
    oxygen_info = "Best"
    oxygen_icon="thumb_up_alt"
    if (float(oxygen)<0.3):
        oxygen_info = "Dangerous"
        oxygen_icon="warning"
    elif (float(oxygen)<4):
        oxygen_info="Good"
        oxygen_icon="check_box"

    #Tilapia can survive in pH ranging from 5 to 10, but optimal pH is between 6 to 9.
    ph_info = "Best"
    ph_icon="thumb_up_alt"
    if (float(ph)<5) or (float(ph)>10):
        ph_info = "Dangerous"
        ph_icon="warning"
    elif (float(oxygen)<6) or (float(ph)>9):
        ph_info="Good"
        ph_icon="check_box"

    return render_template('dashboard.html',temp=temp,temp_icon=temp_icon, temp_info=temp_info, oxygen=oxygen, oxygen_icon=oxygen_icon, oxygen_info=oxygen_info, ph=ph, ph_icon=ph_icon, ph_info=ph_info)

def kill_server():
        print('You pressed Ctrl+C!')
        http_server.stop()
        sys.exit(0)

if __name__ == '__main__':
    init_db()
    #app.run(host='0.0.0.0', port=2000)
    http_server = WSGIServer(('0.0.0.0', 8000), app)
    try:
        http_server.serve_forever()
    except KeyboardInterrupt:
        kill_server()
