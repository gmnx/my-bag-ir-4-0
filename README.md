# My BAG IR 4.0 (Malaysia Bring Aquaculture to Industrial Revolution 4.0)

[![IMAGE ALT TEXT HERE](http://i3.ytimg.com/vi/YcM-o7Re-Fo/hqdefault.jpg)](http://www.youtube.com/watch?v=YcM-o7Re-Fo)

#### This Repo is for Hackathon 2.0 Competition by Ericsson(2019). Using 3 main component for creating solution. To bring Aquaculture in Malaysia to Industrial Revolution 4.0

1. **Floating Device (Sensor Folder)**
    - Arduino for reading sensors (Arduino UNO)
    - Ph Sensor (Atlas Scientific Ph kit)
    - Dissolved Oxygen Sensor (Atlas Scientific DO kit)
    - Waterproof Temperature Sensor (DS18B20)
    - Raspberry for parsing the parameter to the server (Raspberry Pi 3 model B v1.2)
    - Modem for Connectivity with Nb-IoT Network (Neoway N20)
    - Beadboard and jumper
    - Other stuff we can found in Mr DIY

    ![alt text](https://gitlab.com/gmnx/my-bag-ir-4-0/raw/1eb33e85d51591aff9c417c968634ae311313e79/sensor/Schematic%20MyBag_bb.jpg "Logo Title Text 1")

    
2. **Server (Server Folder)**
    - Xpand MQTT Server for message broker. Collect all parameters reading that sent by Raspberry
    - Machine Learning Server for calculate recommendation given to Farmer. Using Xpand API to fetch data

    Dashboard on Xpand http://bit.ly/MyBAG-Dashboard
    
3. **Android (MyBAGIR40 Folder)**
    - Any android device with minimum OS Lolipop, to show pond status with the recommendation given by Machine Learning
