/********************************************************************/
// First we include the libraries
#include <OneWire.h> 
#include <DallasTemperature.h>
#include <SoftwareSerial.h>                           //we have to include the SoftwareSerial library, or else we can't use it
/********************************************************************/
// Data wire is plugged into pin 2 on the Arduino 
#define ONE_WIRE_BUS 6
#define ph_rx 5                                          //define what Ph pin rx is going to be
#define ph_tx 4                                          //define what Ph pin tx is going to be
#define do_rx 2                                          //define what DO pin rx is going to be
#define do_tx 3                                          //define what DO pin tx is going to be
  
/********************************************************************/
// Setup a oneWire instance to communicate with any OneWire devices  
// (not just Maxim/Dallas temperature ICs) 
OneWire oneWire(ONE_WIRE_BUS);
SoftwareSerial phserial(ph_tx, ph_rx);                      //define how the soft serial port is going to work
SoftwareSerial doserial(do_tx, do_rx);                      //define how the soft serial port is going to work

/********************************************************************/
// Ph Variable.
String inputstring = "";                              //a string to hold incoming data from the PC
String phsensorstring = "";                             //a string to hold the data from Ph Atlas Scientific product
String dosensorstring = "";                             //a string to hold the data from DO Atlas Scientific product
boolean input_string_complete = false;                //have we received all the data from the PC
boolean phsensor_string_complete = false;               //have we received all the data from Ph Atlas Scientific product
boolean dosensor_string_complete = false;               //have we received all the data from DO Atlas Scientific product
float pH;                                             //used to hold a floating point number that is the pH
float DO;                                             //used to hold a floating point number that is the DO

 
/********************************************************************/
// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature tempsensors(&oneWire);
/********************************************************************/ 
void setup(void) 
{ 
 // start serial port 
 Serial.begin(9600); 
 // Start up the library 
 tempsensors.begin();
 phserial.begin(9600);                               //set baud rate for the software serial port to 9600
 doserial.begin(9600);                               //set baud rate for the software serial port to 9600
 
 inputstring.reserve(10);                            //set aside some bytes for receiving data from the PC
 phsensorstring.reserve(30);                           //set aside some bytes for receiving data from Ph Atlas Scientific product
 dosensorstring.reserve(30);                           //set aside some bytes for receiving data from DO Atlas Scientific product
}
void serialEvent() {                                  //if the hardware serial port_0 receives a char
  inputstring = Serial.readStringUntil(13);           //read the string until we see a <CR>
  input_string_complete = true;                       //set the flag used to tell if we have received a completed string from the PC
} 
void loop(void) 
{ 
 // call sensors.requestTemperatures() to issue a global temperature 
 // request to all devices on the bus 
/********************************************************************/
 //Serial.print(" Requesting temperatures..."); 
 tempsensors.requestTemperatures(); // Send the command to get temperature readings 
 //Serial.println("DONE"); 
/********************************************************************/
 Serial.print("Temperature = "); 
 Serial.println(tempsensors.getTempCByIndex(0)); // Why "byIndex"?  
   // You can have more than one DS18B20 on the same bus.  
   // 0 refers to the first IC on the wire 
   

if (input_string_complete) {                        //if a string from the PC has been received in its entirety
    phserial.print(inputstring);                      //send that string to Ph Atlas Scientific product
    phserial.print('\r');                             //add a <CR> to the end of the string
    doserial.print(inputstring);                      //send that string to DO Atlas Scientific product
    doserial.print('\r');                             //add a <CR> to the end of the string
    inputstring = "";                                 //clear the string
    input_string_complete = false;                    //reset the flag used to tell if we have received a completed string from the PC
  }
phserial.listen();
delay(1000);
//Serial.println("listening to phsensor");
  while (phserial.available() > 0) {                     //if we see that the Atlas Scientific product has sent a character
    char inchar = (char)phserial.read();              //get the char we just received
    phsensorstring += inchar;                           //add the char to the var called sensorstring
    if (inchar == '\r') {                             //if the incoming character is a <CR>
      phsensor_string_complete = true;                  //set the flag
    }
  }
  
doserial.listen();
delay(1000);
//Serial.println("listening to dosensor");
  while (doserial.available() > 0) {                     //if we see that the Atlas Scientific product has sent a character
    char inchar = (char)doserial.read();              //get the char we just received
    dosensorstring += inchar;                           //add the char to the var called sensorstring
    if (inchar == '\r') {                             //if the incoming character is a <CR>
      dosensor_string_complete = true;                  //set the flag
    }
  }
  //doserial.end();


  if (phsensor_string_complete == true) {               //if a string from the Atlas Scientific product has been received in its entirety
    Serial.print("Ph = ");
    Serial.println(phsensorstring);                     //send that string to the PC's serial monitor
    /*                                                //uncomment this section to see how to convert the pH reading from a string to a float 
    if (isdigit(sensorstring[0])) {                   //if the first character in the string is a digit
      pH = sensorstring.toFloat();                    //convert the string to a floating point number so it can be evaluated by the Arduino
      if (pH >= 7.0) {                                //if the pH is greater than or equal to 7.0
        Serial.println("high");                       //print "high" this is demonstrating that the Arduino is evaluating the pH as a number and not as a string
      }
      if (pH <= 6.999) {                              //if the pH is less than or equal to 6.999
        Serial.println("low");                        //print "low" this is demonstrating that the Arduino is evaluating the pH as a number and not as a string
      }
    }
    */
    phsensorstring = "";                                //clear the string
    phsensor_string_complete = false;                   //reset the flag used to tell if we have received a completed string from the Atlas Scientific product
  }

  if (dosensor_string_complete == true) {               //if a string from the Atlas Scientific product has been received in its entirety
    Serial.print("DO = ");
    Serial.println(dosensorstring);                     //send that string to the PC's serial monitor
    /*                                                //uncomment this section to see how to convert the pH reading from a string to a float 
    if (isdigit(sensorstring[0])) {                   //if the first character in the string is a digit
      pH = sensorstring.toFloat();                    //convert the string to a floating point number so it can be evaluated by the Arduino
      if (pH >= 7.0) {                                //if the pH is greater than or equal to 7.0
        Serial.println("high");                       //print "high" this is demonstrating that the Arduino is evaluating the pH as a number and not as a string
      }
      if (pH <= 6.999) {                              //if the pH is less than or equal to 6.999
        Serial.println("low");                        //print "low" this is demonstrating that the Arduino is evaluating the pH as a number and not as a string
      }
    }
    */
    dosensorstring = "";                                //clear the string
    dosensor_string_complete = false;                   //reset the flag used to tell if we have received a completed string from the Atlas Scientific product
  }
//delay(1000);

    
} 
