#!/usr/bin/env python
import paho.mqtt.client as mqtt
import sys
import time
import json
import threading
import serial
import socket
import fcntl
import struct
import subprocess



DEVICE_SERIAL =  "2673814379082023"#generate the serial number by log in to https://iot.xpand.asia/public/device_management/operating
SERVER_NAME = "mqtt.iot.ideamart.io"
SERVER_PORT = "1883"
KEEPALIVE = 6
EVENT_TOPIC = "generic_brand_1268/generic_device/v1/common"
SUB_TOPIC = "+/" + DEVICE_SERIAL + "/generic_brand_1268/generic_device/v1/sub"
PUB_TOPIC = "/" + DEVICE_SERIAL + "/generic_brand_1268/generic_device/v1/pub"
PORT_COUNT = 28
must_read = True
temperature=""
ph=""
oxygen=""


def connect(tries):
    global client
    print tries
    if tries < 1:
        return

    try:
        client.connect(SERVER_NAME, 1883, 60)

    except:
        print "Can't connect to the server"
        time.sleep(5)
        tries = tries - 1
        connect(tries)


def on_connect(client, userdata, flags, rc):
    print "Connected with Result Code : " + str(rc)
    print "Subscribing to topic " + SUB_TOPIC
    client.subscribe(SUB_TOPIC)
    print "Publish Data"
    data = {}
    data['mac'] = DEVICE_SERIAL
    data['eventName'] = "onDataChange"
    data['status'] = "kentang"
    data['ph'] = ph
    data['do'] = oxygen
    data['temp'] = temperature
    
    try:
        message = json.dumps(data)
        ret=client.publish(EVENT_TOPIC, message, 0, False)
    except IOError as (errno, strerror):
        print "I/O error({0}): {1}".format(errno, strerror)
    except ValueError:
        print "Could not convert data to an integer."
    except:
        print "Unexpected error:", sys.exc_info()[0]


def on_message(client, userdata, msg):
    print "get Action with correlation id :"
    topic = msg.topic
    correlation_id = topic.split('/')
    correlation_id = correlation_id[0]
    print correlation_id
    pubTopic = correlation_id+PUB_TOPIC

    data = json.loads(msg.payload)
    print data
    ip = get_ip_address('wlan0')
    ssid = subprocess.check_output("iwgetid -r", shell=True)
    act_data={}
    act_data['status']="Alive+"+str(ip)+"+"+str(ssid)
    message = json.dumps(act_data)
    ret=client.publish(pubTopic, message, 0, False)
    print ret

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])

def get_onboard_payload():
    serial = DEVICE_SERIAL
    payload = []
    for index in range(PORT_COUNT):
        portIdentifier = serial + "_" + str(index + 1).zfill(2)
        serialContainer = {}
        serialContainer['mac'] = portIdentifier
        payload.append(serialContainer)

    return json.dumps(payload)
    
def read_temp():
    port = '/dev/serial/by-id/usb-Arduino__www.arduino.cc__0043_5573932303735181A1D2-if00'
    ard = serial.Serial(port,9600,timeout=5)
    while must_read:
        msg = ard.readline()
        data = msg.split()
        if len(data)>1:
            if "-127" in data:
                continue
            if "Temperature" in data:
                #print data[2]
                global temperature
                temperature=data[2]
            if "Ph" in data:
                #print data[2]
                global ph
                ph=data[2]
            if "DO" in data:
                #print data[2]
                global oxygen
                oxygen=data[2]


client = mqtt.Client()
client.username_pw_set(username="generic_brand_1268-generic_device-v1_2015", password="1554963528_2015")
client.on_connect = on_connect
client.on_message = on_message
t = threading.Thread(target=read_temp)
t.daemon = True
t.start()

while 1:
    print "Connecting to " + SERVER_NAME + " : " + SERVER_PORT
    connect(5)
    client.loop_start()
    print "Subscribing to topic " + SUB_TOPIC
    ret=client.subscribe(SUB_TOPIC)
    print ret
    # TODO : Update to use a keyboard interrupt
    time.sleep(1)
    client.loop_stop()
    client.disconnect()
